package dawson;
import static org.junit.Assert.assertEquals;
// import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    
    // @Test
    // public void shouldAnswerWithTrue()
    // {
    //     assertTrue( true );
    // }

    @Test
    public void echo() {
        App myApp = new App();
        assertEquals("are they equal", 5, myApp.echo(5));
    }

    @Test
    public void oneMore() {
        App myOtherApp = new App();
        assertEquals("plus 1", 6, myOtherApp.echo(5));
    }
}
